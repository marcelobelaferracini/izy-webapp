FROM node:16.16

WORKDIR /home/app

COPY . .

RUN cp deploy/.env.production .env

RUN cp deploy/.env.production frontend/.env

RUN cp deploy/.env.production functions/.env

RUN mkdir /root/.pm2

RUN cp deploy/ecosystem.config.js /root/.pm2/

RUN apt-get update

RUN npm install

RUN npm install --prefix functions

RUN npm install --prefix frontend

RUN npm install -g pm2

RUN npm run generate --prefix frontend

RUN chmod +x ./entrypoint.sh

EXPOSE 80 3000 3306 5001

ENTRYPOINT ["./entrypoint.sh"]
