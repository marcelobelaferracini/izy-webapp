const nuxtConfig = require('../nuxt.config')

const config = {
    ...nuxtConfig,
    dev: false,
    buildDir: 'nuxt',
}

module.exports = { config }
