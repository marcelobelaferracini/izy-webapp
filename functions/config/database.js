const databaseConfig = {
    client: 'mysql2',
    connection: {
        host: process.env.API_DB_HOST ?? 'localhost',
        port: process.env.API_DB_PORT ?? 3306,
        user: process.env.API_DB_USER ?? 'root',
        password: process.env.API_DB_PASSWORD ?? '',
        database: process.env.API_DB_NAME,
    },
    migrations: {
        directory: './database/migrations',
    },
    pool: {
        min: 2,
        max: 10,
    },
}

module.exports = { databaseConfig }
