/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable('drivers_schools_reviews', function (table) {
        table.increments('id')
        table.text('content').notNullable()
        table.float('rating').notNullable()
        table.integer('users_id').unsigned()
        table.integer('drivers_schools_id').unsigned()
        table.timestamps(true, true, false)
        table.foreign('users_id').references('users.id')
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTable('drivers_schools_reviews')
}
