/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable(
        'drivers_schools_addresses',
        function (table) {
            table.increments('id')
            table.string('street').notNullable()
            table.integer('number').notNullable()
            table.string('neighbourhood', 100).notNullable()
            table.string('city', 100).notNullable()
            table.integer('drivers_schools_id').unsigned()
            table.timestamps(true, true, false)
            table
                .foreign('drivers_schools_id')
                .references('drivers_schools.id')
                .onDelete('CASCADE')
        }
    )
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTable('drivers_schools_addresses')
}
