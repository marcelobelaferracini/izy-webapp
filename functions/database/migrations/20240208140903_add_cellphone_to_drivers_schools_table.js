/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.string('cellphone', 15)
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.dropColumn('cellphone')
    })
}
