/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.integer('external_id')
        table.float('approval_average')
        table.boolean('allow_disabled_people')
        table.boolean('has_recycling')
        table.boolean('has_simulator')
        table.float('average_price_rate')
        table.float('average_rate')
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.dropColumn('external_id')
        table.dropColumn('approval_average')
        table.dropColumn('allow_disabled_people')
        table.dropColumn('has_recycling')
        table.dropColumn('has_simulator')
        table.dropColumn('average_price_rate')
        table.dropColumn('average_rate')
    })
}
