const { hash } = require('bcrypt')

const { useTimestamps } = require('../../src/utils/useTimestamps')

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    const timestamps = useTimestamps(['created_at', 'updated_at'])

    return knex('users').insert([
        {
            name: 'Marcelo Ferracini',
            email: 'marceloferracini@gmail.com',
            password: await hash('admin@izy-drive', 10),
            role: 'admin',
            ...timestamps,
        },
    ])
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {}
