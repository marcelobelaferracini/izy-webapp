/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.string('description').nullable().alter()
        table.string('picture').nullable().alter()
        table.jsonb('licenses').nullable().alter()
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.alterTable('drivers_schools', function (table) {
        table.string('description').notNullable().alter()
        table.string('picture').notNullable().alter()
        table.jsonb('licenses').notNullable().alter()
    })
}
