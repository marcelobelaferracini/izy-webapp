/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable(
        'highlighted_drivers_schools',
        function (table) {
            table.increments('id')
            table.string('state', 100).notNullable()
            table.string('city', 100).notNullable()
            table.jsonb('licenses').notNullable()
            table.integer('drivers_schools_id').unsigned()
            table.timestamps(true, true, false)
            table.foreign('drivers_schools_id').references('drivers_schools.id')
        }
    )
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTable('highlighted_drivers_schools')
}
