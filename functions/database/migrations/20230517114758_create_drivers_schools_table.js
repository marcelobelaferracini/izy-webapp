/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
    return knex.schema.createTable('drivers_schools', function (table) {
        table.increments('id')
        table.string('name').notNullable()
        table.string('telephone', 15).notNullable()
        table.string('description').notNullable()
        table.string('picture').notNullable()
        table.string('slug').notNullable()
        table.jsonb('licenses').notNullable()
        table.timestamps(true, true, false)
    })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
    return knex.schema.dropTable('drivers_schools')
}
