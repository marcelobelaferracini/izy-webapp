const { join } = require('path')

const express = require('express')
const createError = require('http-errors')
const cors = require('cors')
const logger = require('morgan')
require('dotenv').config()

const { DriversSchoolsRouter } = require('./src/routers/DriversSchoolsRouter')
const { AuthRouter } = require('./src/routers/AuthRouter')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use(express.static(join(__dirname, 'public')))

app.use('/', AuthRouter)
app.use('/drivers-schools', DriversSchoolsRouter)

app.use(function (req, res, next) {
    next(new createError.NotFound())
})

app.use(function (err, req, res, next) {
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}
    let error = new createError.InternalServerError(err.message)

    if (err?.status) {
        error = createError(err.status, err.message)
    }

    res.status(error.status).json({ error })
})

module.exports = app
