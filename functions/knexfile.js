const { databaseConfig } = require('./config/database')

// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {
    development: databaseConfig,
    staging: databaseConfig,
    production: databaseConfig,
}
