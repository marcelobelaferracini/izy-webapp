const { databaseConfig } = require('../../config/database')
/**
 * @type { Knex }
 */
const { Knex } = require('knex')
const knex = require('knex')(databaseConfig)

const { FormatFiltersHelper } = require('../helpers/FormatFiltersHelper')
const { RawFilters, Filters } = require('../types')

class Database {
    /**
     * @type {Knex.QueryBuilder<any, DeferredKeySelection<any, never, false, {}, false, {}, never>[]>}
     */
    knex
    /**
     * @type {null}
     */
    transaction

    constructor(model, options = {}) {
        const { transaction = null } = options

        this.knex = knex(model)
        this.transaction = transaction
    }

    get builder() {
        return this.knex
    }

    /**
     * Format raw filters.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Filters}
     */
    getFilters(rawFilters) {
        return FormatFiltersHelper.perform(rawFilters)
    }

    /**
     * Add where clauses to query.
     *
     * @param {Knex.QueryBuilder} query
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Knex.QueryBuilder}
     */
    where(query, rawFilters) {
        /**
         * @type {Filters}
         */
        const filters = this.getFilters(rawFilters)
        const hasWhereFilters = !!filters.where.length

        if (hasWhereFilters) {
            filters.where.forEach((filter) => {
                const isJSON =
                    !!Array.isArray(filter[2]) ||
                    (typeof filter[2] !== 'string' &&
                        !!Object.keys(filter[2]).length)

                if (isJSON) {
                    return query.whereJsonObject(filter[0], filter[2])
                }
                query.where(...filter)
            })
        }

        return query
    }

    /**
     * Add left join clauses to query.
     *
     * @param {Knex.QueryBuilder} query
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Knex.QueryBuilder}
     */
    leftJoin(query, rawFilters) {
        /**
         * @type {Filters}
         */
        const filters = this.getFilters(rawFilters)
        const hasRelationships = !!filters.relationships.length

        if (hasRelationships) {
            filters.relationships.forEach((relationship) => {
                query.leftJoin(...relationship).options({ nestTables: true })
            })
        }

        return query
    }

    /**
     * Add order by clauses to query.
     *
     * @param {Knex.QueryBuilder} query
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Knex.QueryBuilder}
     */
    orderBy(query, rawFilters) {
        /**
         * @type {Filters}
         */
        const filters = this.getFilters(rawFilters)
        const hasOrder = !!filters.order.length

        if (hasOrder) {
            filters.order.forEach((order) => {
                query.orderBy(...order)
            })
        }

        return query
    }

    /**
     * Hydrate rows from database by parsing relationships.
     *
     * @param {Object[]} rows
     * @param {Object} model
     * @param {string} model.name
     * @param {string} model.relationships
     * @returns
     */
    hydrate(rows, model) {
        return rows.map((row) => {
            const hasModelRelationships =
                model.relationships && !!Object.keys(model.relationships).length

            if (row[model.name] && hasModelRelationships) {
                let resource = row[model.name]
                for (const relationship in model.relationships) {
                    const isEmpty = Object.values(row[relationship]).every(
                        (value) => value === null
                    )
                    resource[relationship] = !isEmpty ? row[relationship] : null
                }

                return resource
            }

            return row
        })
    }
}

module.exports = { Database }
