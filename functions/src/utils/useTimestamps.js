const useTimestamps = (columns) => {
    const timestamp = new Date().toISOString().replace('Z', '')

    if (typeof columns === 'string') {
        return {
            [columns]: timestamp,
        }
    }

    if (Array.isArray(columns)) {
        const timestamps = {}

        columns.forEach((column) => {
            timestamps[column] = timestamp
        })

        return timestamps
    }
}

module.exports = { useTimestamps }
