const useJSONParse = (value) => {
    const isJSONRegExp = /(\[.*\])|(\{.*\})/

    if (typeof value === 'object' && isJSONRegExp.test(value[0])) {
        let [match] = value[0].match(isJSONRegExp)

        return JSON.parse(match)
    }

    if (typeof value === 'string' && isJSONRegExp.test(value)) {
        let [match] = value.match(isJSONRegExp)
        match = JSON.parse(match)

        if (typeof match === 'object' && isJSONRegExp.test(match[0])) {
            return JSON.parse(match[0])
        }

        return match
    }

    return value
}

module.exports = { useJSONParse }
