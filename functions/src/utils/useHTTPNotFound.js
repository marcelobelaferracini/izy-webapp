const createError = require('http-errors')

const useHTTPNotFound = (req, res, next) => {
    return next(
        new createError.NotFound(
            `The resource ${req.originalUrl} was not found`
        )
    )
}

module.exports = { useHTTPNotFound }
