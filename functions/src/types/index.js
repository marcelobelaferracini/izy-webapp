/**
 * Represents a user entity.
 *
 * @typedef {Object} User
 * @property {number} id
 * @property {string} name
 * @property {string} email
 * @property {string} password
 * @property {string} role
 * @property {string} created_at
 * @property {string} updated_at
 */

/**
 * Represents a drivers school address entity.
 *
 * @typedef {Object} DriversSchoolAddress
 * @property {number} id
 * @property {string} street
 * @property {string} number
 * @property {string} neighbourhood
 * @property {string} city
 * @property {number} drivers_schools_id
 * @property {string} created_at
 * @property {string} updated_at
 */

/**
 * Represents a drivers school addresses city entity.
 *
 * @typedef {Object} DriversSchoolAddressCity
 * @property {string} city
 */

/**
 * Represents a drivers school addresses neighbourhood entity.
 *
 * @typedef {Object} DriversSchoolAddressNeighbourhood
 * @property {string} neighbourhood
 */

/**
 * Represents a drivers school review entity.
 *
 * @typedef {Object} DriversSchoolReview
 * @property {number} id
 * @property {string} content
 * @property {number} rating
 * @property {number} users_id
 * @property {number} drivers_schools_id
 * @property {string} created_at
 * @property {string} updated_at
 *
 */

/**
 * Represents a drivers school entity.
 *
 * @typedef {Object} DriversSchool
 * @property {number} id
 * @property {string} name
 * @property {string} telephone
 * @property {string} description
 * @property {string} picture
 * @property {string} slug
 * @property {string[]} licenses
 * @property {string} created_at
 * @property {string} updated_at
 * @property {number} external_id
 * @property {number} approval_average
 * @property {boolean} allow_disabled_people
 * @property {boolean} has_recycling
 * @property {boolean} has_simulator
 * @property {boolean} average_price_rate
 * @property {boolean} average_rate
 * @property {(DriversSchoolAddress | null)} [drivers_schools_addresses]
 * @property {(DriversSchoolReview | null)} [drivers_schools_reviews]
 */

/**
 * Represents a highlighted drivers school entity.
 *
 * @typedef {Object} HighlightedDriversSchool
 * @property {number} id
 * @property {string} state
 * @property {string} city
 * @property {string[]} licenses
 * @property {number} drivers_schools_id
 * @property {string} created_at
 * @property {string} updated_at
 * @property {(DriversSchool | null)} [drivers_schools]
 */

/**
 * Represents an HTTP error entity.
 *
 * @typedef {Object} HTTPError
 * @property {Object} error
 * @property {string} error.message
 */

/**
 * Represents raw filters entity.
 * Raw filters from a HTTP's request query has the following format:
 * {
 *   where: [ 'telephone,=,5511942957095' ],
 *   relationships: [ 'drivers_schools_addresses', 'drivers_schools_reviews' ],
 *   limit: '1',
 *   offset: '0'
 * }
 *
 * @typedef {Object} RawFilters
 * @property {string[]} [where]
 * @property {string[]} [relationships]
 * @property {(number | string)} [limit]
 * @property {(number | string)} [offset]
 */

/**
 * Represents a formatted filters entity. It has the following format:
 * {
 *   relationships: [
 *     [
 *       'drivers_schools_addresses',
 *       'drivers_schools.id',
 *       'drivers_schools_addresses.drivers_schools_id'
 *     ],
 *     [
 *       'drivers_schools_reviews',
 *       'drivers_schools.id',
 *       'drivers_schools_reviews.drivers_schools_id'
 *     ]
 *   ],
 *   limit: '1',
 *   offset: '0',
 *   where: [
 *     [ 'telephone', '=', '5511942957095' ]
 *   ]
 * }
 *
 * @typedef {Object} Filters
 * @property {string[]} where
 * @property {string[]} relationships
 * @property {(number | string)} limit
 * @property {(number | string)} offset
 */

module.exports = {}
