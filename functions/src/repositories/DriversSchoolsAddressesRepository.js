const { BaseRepository } = require('./BaseRepository')
const { RawFilters } = require('../types')

class DriversSchoolsAddressesRepository extends BaseRepository {
    constructor() {
        super('drivers_schools_addresses')
    }

    /**
     * Get a list of cities.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Promise<Object[]>}
     */
    async getCities(rawFilters) {
        try {
            const relationships = this.getRelationships(rawFilters)
            const query = this.__get(rawFilters)
            query.distinct('city')
            let rows = await query.select()
            rows = this.database.hydrate(rows, {
                name: this.model,
                relationships,
            })

            return rows
        } catch (error) {
            console.log('DriversSchoolsAddressesRepositoryError', error)

            return
        }
    }

    /**
     * Get a list of neighbourhoods by city.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Promise<Object[]>}
     */
    async getNeighbourhoods(rawFilters) {
        try {
            const relationships = this.getRelationships(rawFilters)
            const query = this.__get(rawFilters)
            query.distinct('neighbourhood')
            let rows = await query.select()
            rows = this.database.hydrate(rows, {
                name: this.model,
                relationships,
            })

            return rows
        } catch (error) {
            console.log('DriversSchoolsAddressesRepositoryError', error)

            return
        }
    }
}

module.exports = { DriversSchoolsAddressesRepository }
