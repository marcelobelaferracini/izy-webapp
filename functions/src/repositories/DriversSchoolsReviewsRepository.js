const { BaseRepository } = require('./BaseRepository')

class DriversSchoolsReviewsRepository extends BaseRepository {
    constructor() {
        super('drivers_schools_reviews')
        this.filters = {
            where: [],
            order: [],
            relationships: {
                drivers_schools: [
                    'drivers_schools',
                    'drivers_schools_reviews.drivers_schools_id',
                    'drivers_schools.id',
                ],
                users: [
                    'users',
                    'drivers_schools_reviews.users_id',
                    'users.id',
                ],
            },
            limit: 100,
            offset: 0,
        }
    }
}

module.exports = { DriversSchoolsReviewsRepository }
