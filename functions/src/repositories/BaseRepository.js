const { Knex } = require('knex')

const { Database } = require('../facades/Database')
const { useTimestamps } = require('../utils/useTimestamps')
const { RawFilters, Filters } = require('../types')

class BaseRepository {
    database
    model
    filters

    constructor(model) {
        this.model = model
        this.filters = {
            where: [],
            order: [],
            relationships: {},
            limit: 100,
            offset: 0,
        }
        this.database = new Database(this.model)
    }

    /**
     * Returns a Knex query builder instance.
     * @returns {Knex.QueryBuilder<any, DeferredKeySelection<any, never, false, {}, false, {}, never>[]>}
     */
    get builder() {
        return this.database.knex
    }

    /**
     * Format relationships.
     * Formatted relationships has the following format:
     * {
     *   drivers_schools_addresses: [
     *    'drivers_schools_addresses',
     *    'drivers_schools.id',
     *    'drivers_schools_addresses.drivers_schools_id'
     *   ],
     *   drivers_schools_reviews: [
     *    'drivers_schools_reviews',
     *    'drivers_schools.id',
     *    'drivers_schools_reviews.drivers_schools_id'
     *   ]
     * }
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Promise<Object>}
     */
    getRelationships(rawFilters) {
        const relationships = {}
        const hasRelatishipFilter =
            rawFilters?.relationships && !!rawFilters.relationships?.length

        if (hasRelatishipFilter) {
            rawFilters.relationships.forEach((relationship) => {
                if (this.filters.relationships[relationship]) {
                    relationships[relationship] =
                        this.filters.relationships[relationship]
                }
            })
        }

        if (!Object.keys(relationships).length) {
            return null
        }

        return relationships
    }

    /**
     * Store the related model.
     *
     * @param {Object} data
     * @returns {Promise<Object>}
     */
    async __create(data) {
        try {
            const timestamps = useTimestamps(['created_at', 'updated_at'])

            const [id] = await this.database.builder.insert({
                ...data,
                ...timestamps,
            })

            return {
                id,
                ...data,
                ...timestamps,
            }
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Store the related model.
     *
     * @param {Object} data
     * @returns {Promise<Object>}
     */
    async create(data) {
        return this.__create(data)
    }

    /**
     * Build a query based on rawFilters and return the query instance.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Knex.QueryBuilder}
     */
    __get(rawFilters) {
        try {
            const relationships = this.getRelationships(rawFilters)
            /**
             * @type {Filters}
             */
            const filters = this.database.getFilters({
                ...rawFilters,
                relationships,
            })
            let query = this.database.builder
                .limit(filters.limit)
                .offset(filters.offset)
            this.database.where(query, rawFilters)
            this.database.orderBy(query, rawFilters)

            if (relationships) {
                this.database.leftJoin(query, filters)
            }

            return query
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Get a list of the related model.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Promise<Object[]>}
     */
    async get(rawFilters) {
        try {
            const relationships = this.getRelationships(rawFilters)
            const query = this.__get(rawFilters)
            let rows = await query.select()
            rows = this.database.hydrate(rows, {
                name: this.model,
                relationships,
            })

            return rows
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Get the first model by id or where field == value.
     *
     * @param {number} id
     * @param {RawFilters} [rawFilters={}]
     * @see {@link RawFilters} for further information.
     * @returns {Promise<(null | Object)>}
     */
    async getFirst(id, rawFilters = {}) {
        try {
            const relationships = this.getRelationships(rawFilters)
            /**
             * @type {Filters}
             */
            const filters = this.database.getFilters({
                ...rawFilters,
                relationships,
            })
            let query = this.database.builder
                .limit(filters.limit)
                .offset(filters.offset)
            const hasWhereFilters = !!filters.where.length

            if (hasWhereFilters) {
                this.database.where(query, rawFilters)
            } else {
                query = this.database.builder.where(`${this.model}.id`, '=', id)
            }

            if (relationships) {
                this.database.leftJoin(query, { ...rawFilters, relationships })
            }

            let row = await query.first()

            if (!row) {
                return null
            }

            let [result] = this.database.hydrate([row], {
                name: this.model,
                relationships,
            })

            return result
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Update the related model by id or by where filters.
     *
     * @param {number} id
     * @param {Object} data
     * @param {RawFilters} [rawFilters={}]
     * @see {@link RawFilters} for further information.
     * @returns {Promise<(null | Object)>}
     */
    async __update(id, data, rawFilters = {}) {
        try {
            let resource = null
            let query = null
            const hasFilters = !!Object.keys(rawFilters).length
            const timestamps = useTimestamps('updated_at')

            if (hasFilters) {
                const { limit = 1 } = rawFilters
                resource = (await this.get({ ...rawFilters, limit }))[0]
                query = await this.database
                    .where(this.database.builder, rawFilters)
                    .update({ ...data, ...timestamps })
            } else {
                resource = await this.getFirst(id, rawFilters)
                query = await this.database.builder
                    .where('id', '=', id)
                    .update({ ...data, ...timestamps })
            }

            if (!query) {
                return null
            }

            return {
                id,
                ...resource,
                ...data,
            }
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Update the related model by id or by where filters.
     *
     * @param {number} id
     * @param {Object} data
     * @param {RawFilters} [rawFilters={}]
     * @see {@link RawFilters} for further information.
     * @returns {Promise<(null | Object)>}
     */
    async update(id, data, rawFilters = {}) {
        return this.__update(id, data, rawFilters)
    }

    /**
     * Delete the related model by id.
     *
     * @param {number} id
     * @returns {Promise<(null | Object)>}
     */
    async delete(id) {
        try {
            const query = await this.database.builder.where('id', '=', id).del()

            if (!query) {
                return null
            }

            return { id }
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }

    /**
     * Store or update the related model based on column
     *
     * @param {Object} data
     * @param {string} column
     * @returns {Promise<Object>}
     */
    async createOrUpdate(data, column) {
        try {
            const timestamps = useTimestamps(['created_at', 'updated_at'])

            for (const [key, value] of Object.entries(data)) {
                if (typeof value === 'object') {
                    data[key] = JSON.stringify(value)
                }
            }

            const [id] = await this.database.builder
                .insert({
                    ...data,
                    ...timestamps,
                })
                .onConflict(column)
                .merge()

            return {
                id,
                ...data,
                ...timestamps,
            }
        } catch (error) {
            console.log('BaseRepositoryError', error)

            return
        }
    }
}

module.exports = { BaseRepository }
