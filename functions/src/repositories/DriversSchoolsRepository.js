const { Knex } = require('knex')

const { BaseRepository } = require('./BaseRepository')
const { useJSONParse } = require('../utils/useJSONParse')
const { RawFilters, Filters } = require('../types')

class DriversSchoolsRepository extends BaseRepository {
    constructor() {
        super('drivers_schools')
        this.filters = {
            where: [],
            order: [],
            relationships: {
                drivers_schools_addresses: [
                    'drivers_schools_addresses',
                    'drivers_schools.id',
                    'drivers_schools_addresses.drivers_schools_id',
                ],
                drivers_schools_reviews: [
                    'drivers_schools_reviews',
                    'drivers_schools.id',
                    'drivers_schools_reviews.drivers_schools_id',
                ],
            },
            limit: 100,
            offset: 0,
        }
    }

    /**
     * Build a query based on rawFilters and return the query instance.
     *
     * @param {RawFilters} rawFilters
     * @see {@link RawFilters} for further information.
     * @returns {Knex.QueryBuilder}
     */
    __get(rawFilters) {
        try {
            const relationships = this.getRelationships(rawFilters)
            /**
             * @type {Filters}
             */
            const filters = this.database.getFilters({
                ...rawFilters,
                relationships,
            })
            let query = this.database.builder
                .limit(filters.limit)
                .offset(filters.offset)
            this.database.where(query, rawFilters)

            filters.order.forEach((order) => {
                const [field, operator] = order

                if (field === 'random') {
                    this.database.builder.orderByRaw('RAND()')
                    rawFilters.order = rawFilters.order.filter(
                        (order) => order !== 'random'
                    )
                }

                if (
                    field === 'average_price_rate' &&
                    operator.toUpperCase() === 'ASC'
                ) {
                    this.database.builder.orderByRaw(
                        'average_price_rate = 0 ASC'
                    )
                }
            })

            this.database.orderBy(query, rawFilters)

            if (relationships) {
                this.database.leftJoin(query, filters)
            }

            return query
        } catch (error) {
            console.log('DriversSchoolsRepositoryError', error)

            return
        }
    }

    /**
     * Store the related model.
     *
     * @param {Object} data
     * @returns {Promise<Object>}
     */
    async create(data) {
        try {
            if (!('licenses' in data)) {
                data.licenses = []
            }

            data.licenses = JSON.stringify(data.licenses)
            const result = await this.__create(data)

            return {
                ...result,
                licenses: useJSONParse(result.licenses),
            }
        } catch (error) {
            console.log('DriversSchoolsRepositoryError', error)

            return
        }
    }

    /**
     * Update the related model by id or by where filters.
     *
     * @param {number} id
     * @param {Object} data
     * @param {RawFilters} [rawFilters={}]
     * @see {@link RawFilters} for further information.
     * @returns {Promise<(null | Object)>}
     */
    async update(id, data, rawFilters = {}) {
        try {
            if (!('licenses' in data)) {
                data.licenses = []
            }

            data.licenses = JSON.stringify(data.licenses)
            const result = await this.__update(id, data, rawFilters)

            if (!result) {
                return null
            }

            return {
                ...result,
                licenses: useJSONParse(result.licenses),
            }
        } catch (error) {
            console.log('DriversSchoolsRepositoryError', error)

            return
        }
    }
}

module.exports = { DriversSchoolsRepository }
