const { BaseRepository } = require('./BaseRepository')

class UsersRepository extends BaseRepository {
    constructor() {
        super('users')
    }
}

module.exports = { UsersRepository }
