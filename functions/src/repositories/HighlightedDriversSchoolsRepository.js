const { Database } = require('../facades/Database')
const { DriversSchoolsRepository } = require('./DriversSchoolsRepository')

class HighlightedDriversSchoolsRepository extends DriversSchoolsRepository {
    constructor() {
        const model = 'highlighted_drivers_schools'
        super(model)
        this.model = model
        this.filters = {
            where: [],
            order: [],
            relationships: {
                drivers_schools: [
                    'drivers_schools',
                    'highlighted_drivers_schools.drivers_schools_id',
                    'drivers_schools.id',
                ],
                drivers_schools_addresses: [
                    'drivers_schools_addresses',
                    'highlighted_drivers_schools.drivers_schools_id',
                    'drivers_schools_addresses.drivers_schools_id',
                ],
            },
            limit: 100,
            offset: 0,
        }
        this.database = new Database(this.model)
    }
}

module.exports = { HighlightedDriversSchoolsRepository }
