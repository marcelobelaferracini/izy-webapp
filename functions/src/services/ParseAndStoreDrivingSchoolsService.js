const csv = require('csv-parser')
const fs = require('fs')
const slug = require('slug')

const {
    DriversSchoolsRepository,
} = require('../repositories/DriversSchoolsRepository')

const {
    DriversSchoolsAddressesRepository,
} = require('../repositories/DriversSchoolsAddressesRepository')

class ParseAndStoreDrivingSchoolsService {
    /**
     * Parse the driving schools and vehicles .csv files and stores it in the database
     */
    async parseAndStore() {
        try {
            const parsedDrivingSchools = []

            // first parses all the driving schools and pushes it to parsedDrivingSchools
            fs.createReadStream('../../files/driving-schools.csv')
                .pipe(csv())
                .on('data', (parsedDrivingSchool) => {
                    parsedDrivingSchool.licenses = []
                    parsedDrivingSchools.push(parsedDrivingSchool)
                })
                .on('end', async () => {
                    // after finished to parse the driving schools, parses the vehicles to extract their licenses
                    fs.createReadStream('../../files/vehicles.csv')
                        .pipe(csv())
                        .on('data', (vehicle) => {
                            const existingLicenses = ['A', 'B', 'C', 'D', 'E']
                            const license = vehicle.category
                                ? vehicle.category.charAt(0)
                                : ''
                            const parsedDrivingSchool =
                                parsedDrivingSchools.find(
                                    (item) => item.id == vehicle.cfc_id
                                )

                            if (
                                parsedDrivingSchool &&
                                existingLicenses.includes(license) &&
                                !parsedDrivingSchool.licenses.includes(license)
                            ) {
                                parsedDrivingSchool.licenses.push(license)
                            }
                        })
                        .on('end', async () => {
                            // after all stores the parsed driving schools in the database
                            this.#storeDrivingSchool(parsedDrivingSchools)
                            console.log('Finished.')
                        })
                })
        } catch (error) {
            console.error('Error parsing and storing CSV:', error)
        }
    }

    /**
     * Create or update the parsed driving schools
     *
     * @param {object[]} parsedDrivingSchools
     */
    async #storeDrivingSchool(parsedDrivingSchools) {
        parsedDrivingSchools.forEach(async (parsedDrivingSchool) => {
            const driversSchoolsRepository = new DriversSchoolsRepository()
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()

            const drivingSchool = {
                external_id: parsedDrivingSchool.id,
                approval_average: !isNaN(parsedDrivingSchool.approval_average)
                    ? parsedDrivingSchool.approval_average
                    : 0.0,
                name: parsedDrivingSchool.name,
                telephone: parsedDrivingSchool.phone_number,
                allow_disabled_people:
                    parsedDrivingSchool.allow_disabled_people == 'Sim',
                has_recycling: parsedDrivingSchool.has_recycling == 'Sim',
                has_simulator: parsedDrivingSchool.has_simulator == 'Sim',
                average_price_rate: !isNaN(
                    parsedDrivingSchool.average_price_rate
                )
                    ? parsedDrivingSchool.average_price_rate
                    : 0.0,
                average_rate: !isNaN(parsedDrivingSchool.average_rate)
                    ? parsedDrivingSchool.average_rate
                    : 0.0,
                slug: slug(
                    `${parsedDrivingSchool.id}-${parsedDrivingSchool.name}`
                ),
                licenses: parsedDrivingSchool.licenses,
            }

            const createdDrivingSchool =
                await driversSchoolsRepository.createOrUpdate(
                    drivingSchool,
                    'external_id'
                )

            const drivingSchoolAddress = {
                street: parsedDrivingSchool.address,
                number: parsedDrivingSchool.number,
                neighbourhood: parsedDrivingSchool.neighborhood,
                city: parsedDrivingSchool.city,
                drivers_schools_id: createdDrivingSchool.id,
            }

            await driversSchoolsAddressesRepository.createOrUpdate(
                drivingSchoolAddress,
                'drivers_schools_id'
            )
        })
    }
}

module.exports = ParseAndStoreDrivingSchoolsService
