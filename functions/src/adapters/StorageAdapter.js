const {
    getStorage,
    ref,
    uploadBytes,
    getDownloadURL,
} = require('firebase/storage')

class StorageAdapter {
    static save(filenamePath, file, options = {}) {
        const storage = getStorage()
        const storageRef = ref(storage, filenamePath)
        return uploadBytes(storageRef, file, options)
    }

    static async getURL(filenamePath) {
        const storage = getStorage()
        const storageRef = ref(storage, filenamePath)
        return await getDownloadURL(storageRef)
    }
}

module.exports = { StorageAdapter }
