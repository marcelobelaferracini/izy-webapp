const { Request, Response, NextFunction } = require('express')
const { compare } = require('bcrypt')
const createError = require('http-errors')
const { sign } = require('jsonwebtoken')

const { UsersRepository } = require('../repositories/UsersRepository')
const { useHTTPNotFound } = require('../utils/useHTTPNotFound')
const { User, HTTPError } = require('../types')

class AuthController {
    /**
     * @typedef {Object} AuthenticateResponse
     * @property {string} token
     */

    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be AuthenticateResponse.
     * In 401 status code scenario, the http's body will be HTTPError.
     * In 403 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(AuthenticateResponse | HTTPError)>} res
     * @param {NextFunction} next
     * @returns {Response<(AuthenticateResponse | HTTPError)>}
     */
    static async authenticate(req, res, next) {
        try {
            const { email = '', password = '' } = req.body
            const usersRepository = new UsersRepository()
            /**
             * @type {(User | null)}
             */
            const user = await usersRepository.getFirst(0, {
                where: [['email', '=', email]],
            })

            if (!user) {
                return next(
                    new createError.Unauthorized('Invalid credentials.')
                )
            }

            /**
             * @type {boolean}
             */
            const isPasswordValid = await compare(password, user.password)

            if (!isPasswordValid) {
                return next(
                    new createError.Unauthorized('Invalid credentials.')
                )
            }

            /**
             * @type {string}
             */
            const access_token = sign(
                {
                    id: user.id,
                    email: user.email,
                    role: user.role,
                },
                process.env.API_KEY,
                {
                    issuer: email,
                    audience: process.env.API_URL,
                }
            )

            return res.status(200).send({
                access_token,
            })
        } catch (error) {
            console.log('AuthControllerError', error)

            next(error)
        }
    }
}

module.exports = { AuthController }
