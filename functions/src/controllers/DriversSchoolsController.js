const { Request, Response, NextFunction } = require('express')
const slug = require('slug')

const {
    DriversSchoolsRepository,
} = require('../repositories/DriversSchoolsRepository')
const {
    DriversSchoolsAddressesRepository,
} = require('../repositories/DriversSchoolsAddressesRepository')
const { useHTTPNotFound } = require('../utils/useHTTPNotFound')
const { DriversSchool, DriversSchoolAddress, HTTPError } = require('../types')

class DriversSchoolsController {
    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be DriversSchool[].
     *
     * @param {Request} req
     * @param {Response<DriversSchool[]>} res
     * @param {NextFunction} next
     * @see {@link DriversSchool} for further information.
     * @returns {Response<DriversSchool[]>}
     */
    static async index(req, res, next) {
        try {
            const driversSchoolsRepository = new DriversSchoolsRepository()
            /**
             * @type {DriversSchool[]}
             */
            const driversSchools = await driversSchoolsRepository.get(req.query)

            return res.status(200).send(driversSchools)
        } catch (error) {
            console.log('DriversSchoolControllerError', error)

            next(error)
        }
    }

    /**
     * Display the specified resource.
     * In 200 status code scenario, the http's body will be DriversSchool.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(DriversSchool | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchool} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchool | HTTPError)>}
     */
    static async show(req, res, next) {
        try {
            const driversSchoolsRepository = new DriversSchoolsRepository()
            /**
             * @type {(DriversSchool | null)}
             */
            const driversSchool = await driversSchoolsRepository.getFirst(
                req.params.id,
                req.query
            )

            if (!driversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchool)
        } catch (error) {
            console.log('DriversSchoolControllerError', error)

            next(error)
        }
    }

    /**
     * Create a new resource.
     * In 201 status code scenario, the http's body will be DriversSchool.
     *
     * @param {Request} req
     * @param {Response<DriversSchool>} res
     * @param {NextFunction} next
     * @see {@link DriversSchool} for further information.
     * @returns {Response<DriversSchool>}
     */
    static async store(req, res, next) {
        try {
            const [basePath, filePath] = req.file.destination.split('public')
            const picture =
                process.env.API_URL + filePath + '/' + req.file.filename

            const { street, number, neighbourhood, city, ...body } = req.body
            const id = Date.now()
            const driversSchoolsRepository = new DriversSchoolsRepository()
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            const driversSchoolSlug = slug(`${id}-${body.name}`)
            /**
             * @type {DriversSchool}
             */
            const driversSchool = await driversSchoolsRepository.create({
                ...body,
                picture,
                slug: driversSchoolSlug,
            })
            /**
             * @type {DriversSchoolAddress}
             */
            const driversSchoolAddress =
                await driversSchoolsAddressesRepository.create({
                    street,
                    number,
                    neighbourhood,
                    city,
                    drivers_schools_id: driversSchool.id,
                })

            return res.status(201).send({
                ...driversSchool,
                drivers_school_address: driversSchoolAddress,
            })
        } catch (error) {
            console.log('DriversSchoolControllerError', error)

            next(error)
        }
    }

    /**
     * Update the specified resource in storage.
     * In 200 status code scenario, the http's body will be DriversSchool.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(DriversSchool | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchool} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchool | HTTPError)>}
     */
    static async update(req, res, next) {
        try {
            const { street, number, neighbourhood, city, ...body } = req.body
            const driversSchoolsRepository = new DriversSchoolsRepository()
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            let picture = ''

            if (body.picture) picture = body.picture

            if (req.file) {
                const [basePath, filePath] =
                    req.file.destination.split('public')
                picture =
                    process.env.API_URL + filePath + '/' + req.file.filename
            }

            /**
             * @type {(DriversSchool | null)}
             */
            const driversSchool = await driversSchoolsRepository.update(
                req.params.id,
                {
                    ...body,
                    picture,
                }
            )

            if (!driversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            /**
             * @type {DriversSchoolAddress}
             */
            const driversSchoolAddress =
                await driversSchoolsAddressesRepository.update(
                    driversSchool.id,
                    {
                        street,
                        number,
                        neighbourhood,
                        city,
                        drivers_schools_id: driversSchool.id,
                    },
                    {
                        where: [['drivers_schools_id', '=', driversSchool.id]],
                    }
                )

            return res.status(200).send({
                ...driversSchool,
                drivers_school_address: driversSchoolAddress,
            })
        } catch (error) {
            console.log('DriversSchoolControllerError', error)

            next(error)
        }
    }

    /**
     * Remove the specified resource from storage.
     * In 200 status code scenario, the http's body will be an empty object.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<({} | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HTTPError} for further information.
     * @returns {Response<({} | HTTPError)>}
     */
    static async destroy(req, res, next) {
        try {
            const driversSchoolsRepository = new DriversSchoolsRepository()
            const driversSchool = await driversSchoolsRepository.delete(
                req.params.id
            )
            /**
             * @type {(DriversSchool | null)}
             */
            if (!driversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchool)
        } catch (error) {
            console.log('DriversSchoolControllerError', error)

            next(error)
        }
    }
}

module.exports = { DriversSchoolsController }
