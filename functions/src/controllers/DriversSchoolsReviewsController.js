const { Request, Response, NextFunction } = require('express')

const { UsersRepository } = require('../repositories/UsersRepository')
const {
    DriversSchoolsRepository,
} = require('../repositories/DriversSchoolsRepository')
const {
    DriversSchoolsReviewsRepository,
} = require('../repositories/DriversSchoolsReviewsRepository')
const { useHTTPNotFound } = require('../utils/useHTTPNotFound')
const {
    DriversSchool,
    DriversSchoolReview,
    User,
    HTTPError,
} = require('../types')

class DriversSchoolsReviewsController {
    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be DriversSchoolReview[].
     *
     * @param {Request} req
     * @param {Response<DriversSchoolReview[]>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolReview} for further information.
     * @returns {Response<DriversSchoolReview[]>}
     */
    static async index(req, res, next) {
        try {
            const driversSchoolsReviewsRepository =
                new DriversSchoolsReviewsRepository()
            /**
             * @type {DriversSchoolReview[]}
             */
            const driversSchoolsReviews =
                await driversSchoolsReviewsRepository.get(req.query)

            return res.status(200).send(driversSchoolsReviews)
        } catch (error) {
            console.log('DriversSchoolsReviewsControllerError', error)

            next(error)
        }
    }

    /**
     * Display the specified resource.
     * In 200 status code scenario, the http's body will be DriversSchoolReview.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(DriversSchoolReview | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolReview} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchoolReview | HTTPError)>}
     */
    static async show(req, res, next) {
        try {
            const driversSchoolsReviewsRepository =
                new DriversSchoolsReviewsRepository()
            /**
             * @type {(DriversSchoolReview | null)}
             */
            const driversSchoolReview =
                await driversSchoolsReviewsRepository.getFirst(
                    req.params.id,
                    req.query
                )

            if (!driversSchoolReview) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchoolReview)
        } catch (error) {
            console.log('DriversSchoolsReviewsControllerError', error)

            next(error)
        }
    }

    /**
     * Create a new resource.
     * In 201 status code scenario, the http's body will be DriversSchoolReview.
     *
     * @param {Request} req
     * @param {Response<DriversSchoolReview>} res
     * @param {NextFunction} next
     * @see {@link DriversSchool} for further information.
     * @returns {Response<DriversSchoolReview>}
     */
    static async store(req, res, next) {
        try {
            const { users_id, ...body } = req.body
            const usersRepository = new UsersRepository()
            const driversSchoolsRepository = new DriversSchoolsRepository()
            const driversSchoolsReviewsRepository =
                new DriversSchoolsReviewsRepository()

            /**
             * @type {(User | null)}
             */
            const user = await usersRepository.getFirst(users_id)

            if (!user) {
                return useHTTPNotFound(req, res, next)
            }

            /**
             * @type {(DriversSchool | null)}
             */
            const driversSchool = await driversSchoolsRepository.getFirst(
                req.params.id
            )

            if (!driversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            /**
             * @type {(DriversSchoolReview)}
             */
            const driversSchoolReview =
                await driversSchoolsReviewsRepository.create({
                    ...body,
                    users_id,
                    drivers_schools_id: req.params.id,
                })

            return res.status(201).send(driversSchoolReview)
        } catch (error) {
            console.log('DriversSchoolsReviewsControllerError', error)

            next(error)
        }
    }

    /**
     * Update the specified resource in storage.
     * In 200 status code scenario, the http's body will be DriversSchoolReview.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(DriversSchoolReview | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolReview} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchoolReview | HTTPError)>}
     */
    static async update(req, res, next) {
        try {
            const driversSchoolsReviewsRepository =
                new DriversSchoolsReviewsRepository()
            /**
             * @type {(DriversSchoolReview | null)}
             */
            const driversSchoolReview =
                await driversSchoolsReviewsRepository.update(
                    req.params.id,
                    req.body
                )

            if (!driversSchoolReview) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchoolReview)
        } catch (error) {
            console.log('DriversSchoolsReviewsControllerError', error)

            next(error)
        }
    }

    /**
     * Remove the specified resource from storage.
     * In 200 status code scenario, the http's body will be an empty object.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<({} | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HTTPError} for further information.
     * @returns {Response<({} | HTTPError)>}
     */
    static async destroy(req, res, next) {
        try {
            const driversSchoolsReviewsRepository =
                new DriversSchoolsReviewsRepository()
            const driversSchoolReview =
                await driversSchoolsReviewsRepository.delete(req.params.id)
            /**
             * @type {(DriversSchoolReview | null)}
             */
            if (!driversSchoolReview) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchoolReview)
        } catch (error) {
            console.log('DriversSchoolsReviewsControllerError', error)

            next(error)
        }
    }
}

module.exports = { DriversSchoolsReviewsController }
