const { Request, Response, NextFunction } = require('express')

const {
    DriversSchoolsAddressesRepository,
} = require('../repositories/DriversSchoolsAddressesRepository')
const { useHTTPNotFound } = require('../utils/useHTTPNotFound')
const {
    DriversSchoolAddress,
    DriversSchoolAddressCity,
    DriversSchoolAddressNeighbourhood,
    HTTPError,
} = require('../types')

class DriversSchoolsAddressesController {
    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be DriversSchoolAddress[].
     *
     * @param {Request} req
     * @param {Response<DriversSchoolAddress[]>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddress} for further information.
     * @returns {Response<DriversSchoolAddress[]>}
     */
    static async index(req, res, next) {
        try {
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            /**
             * @type {DriversSchoolAddress[]}
             */
            const driversSchoolsAddresses =
                await driversSchoolsAddressesRepository.get(req.query)

            return res.status(200).send(driversSchoolsAddresses)
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be DriversSchoolAddressCity[].
     *
     * @param {Request} req
     * @param {Response<DriversSchoolAddressCity[]>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddressCity} for further information.
     * @returns {Response<DriversSchoolAddressCity[]>}
     */
    static async getCities(req, res, next) {
        try {
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            /**
             * @type {DriversSchoolAddressCity[]}
             */
            const driversSchoolsAddressesCities =
                await driversSchoolsAddressesRepository.getCities(req.query)
            return res.status(200).send(driversSchoolsAddressesCities)
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be DriversSchoolAddressNeighbourhood[].
     *
     * @param {Request} req
     * @param {Response<DriversSchoolAddressNeighbourhood[]>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddressNeighbourhood} for further information.
     * @returns {Response<DriversSchoolAddressNeighbourhood[]>}
     */
    static async getNeighbourhoods(req, res, next) {
        try {
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            /**
             * @type {DriversSchoolAddressNeighbourhood[]}
             */
            const driversSchoolsAddressesCities =
                await driversSchoolsAddressesRepository.getNeighbourhoods(
                    req.query
                )
            return res.status(200).send(driversSchoolsAddressesCities)
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Display the specified resource.
     * In 200 status code scenario, the http's body will be DriversSchoolAddress.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(DriversSchoolAddress | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddress} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchoolAddress | HTTPError)>}
     */
    static async show(req, res, next) {
        try {
            const driversSchoolsAddressesRepository =
                new DriversSchoolsAddressesRepository()
            /**
             * @type {(DriversSchoolAddress | null)}
             */
            const driversSchoolsAddress =
                await driversSchoolsAddressesRepository.getFirst(
                    req.params.id,
                    req.query
                )

            if (!driversSchoolsAddress) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(driversSchoolsAddress)
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Create a new resource.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<DriversSchoolAddress>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddress} for further information.
     * @returns {Response<DriversSchoolAddress>}
     */
    static async store(req, res, next) {
        try {
            return res.status(200).send({})
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Update the specified resource in storage.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<(DriversSchoolAddress | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link DriversSchoolAddress} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(DriversSchoolAddress | HTTPError)>}
     */
    static async update(req, res, next) {
        try {
            return res.status(200).send({})
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }

    /**
     * Remove the specified resource from storage.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<({} | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HTTPError} for further information.
     * @returns {Response<({} | HTTPError)>}
     */
    static async destroy(req, res, next) {
        try {
            return res.status(200).send({})
        } catch (error) {
            console.log('DriversSchoolsAddressesControllerError', error)

            next(error)
        }
    }
}

module.exports = { DriversSchoolsAddressesController }
