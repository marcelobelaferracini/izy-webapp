const { Request, Response, NextFunction } = require('express')

const {
    HighlightedDriversSchoolsRepository,
} = require('../repositories/HighlightedDriversSchoolsRepository')
const { useHTTPNotFound } = require('../utils/useHTTPNotFound')
const { HighlightedDriversSchool, HTTPError } = require('../types')

class HighlightedDriversSchoolsController {
    /**
     * Display a listing of the resource.
     * In 200 status code scenario, the http's body will be HighlightedDriversSchool[].
     *
     * @param {Request} req
     * @param {Response<HighlightedDriversSchool[]>} res
     * @param {NextFunction} next
     * @see {@link HighlightedDriversSchool} for further information.
     * @returns {Response<HighlightedDriversSchool[]>}
     */
    static async index(req, res, next) {
        try {
            const highlightedDriversSchoolsRepository =
                new HighlightedDriversSchoolsRepository()
            /**
             * @type {HighlightedDriversSchool[]}
             */
            const highlightedDriversSchools =
                await highlightedDriversSchoolsRepository.get(req.query)

            return res.status(200).send(highlightedDriversSchools)
        } catch (error) {
            console.log('HighlightedDriversSchoolsControllerError', error)

            next(error)
        }
    }

    /**
     * Display the specified resource.
     * In 200 status code scenario, the http's body will be HighlightedDriversSchool.
     * In 404 status code scenario, the http's body will be HTTPError.
     *
     * @param {Request} req
     * @param {Response<(HighlightedDriversSchool | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HighlightedDriversSchool} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(HighlightedDriversSchool | HTTPError)>}
     */
    static async show(req, res, next) {
        try {
            const highlightedDriversSchoolsRepository =
                new HighlightedDriversSchoolsRepository()
            /**
             * @type {(HighlightedDriversSchool | null)}
             */
            const highlightedDriversSchool =
                await highlightedDriversSchoolsRepository.get(req.query)

            if (!highlightedDriversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(highlightedDriversSchool)
        } catch (error) {
            console.log('HighlightedDriversSchoolsControllerError', error)

            next(error)
        }
    }

    /**
     * Create a new resource.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<HighlightedDriversSchool>} res
     * @param {NextFunction} next
     * @see {@link HighlightedDriversSchool} for further information.
     * @returns {Response<HighlightedDriversSchool>}
     */
    static async store(req, res, next) {
        try {
            const highlightedDriversSchoolsRepository =
                new HighlightedDriversSchoolsRepository()
            /**
             * @type {HighlightedDriversSchool}
             */
            const highlightedDriversSchool =
                await highlightedDriversSchoolsRepository.create(req.body)

            return res.status(201).send(highlightedDriversSchool)
        } catch (error) {
            console.log('HighlightedDriversSchoolsControllerError', error)

            next(error)
        }
    }

    /**
     * Update the specified resource in storage.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<(HighlightedDriversSchool | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HighlightedDriversSchool} for further information.
     * @see {@link HTTPError} for further information.
     * @returns {Response<(HighlightedDriversSchool | HTTPError)>}
     */
    static async update(req, res, next) {
        try {
            const highlightedDriversSchoolsRepository =
                new HighlightedDriversSchoolsRepository()
            /**
             * @type {(HighlightedDriversSchool | null)}
             */
            const highlightedDriversSchool =
                await highlightedDriversSchoolsRepository.update(
                    req.params.id,
                    req.body
                )

            if (!highlightedDriversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(highlightedDriversSchool)
        } catch (error) {
            console.log('HighlightedDriversSchoolsControllerError', error)

            next(error)
        }
    }

    /**
     * Remove the specified resource from storage.
     * In 200 status code scenario, the http's body will be an empty object.
     *
     * @param {Request} req
     * @param {Response<({} | HTTPError)>} res
     * @param {NextFunction} next
     * @see {@link HTTPError} for further information.
     * @returns {Response<({} | HTTPError)>}
     */
    static async destroy(req, res, next) {
        try {
            const highlightedDriversSchoolsRepository =
                new HighlightedDriversSchoolsRepository()
            /**
             * @type {(HighlightedDriversSchool | null)}
             */
            const highlightedDriversSchool =
                await highlightedDriversSchoolsRepository.delete(req.params.id)

            if (!highlightedDriversSchool) {
                return useHTTPNotFound(req, res, next)
            }

            return res.status(200).send(highlightedDriversSchool)
        } catch (error) {
            console.log('HighlightedDriversSchoolsControllerError', error)

            next(error)
        }
    }
}

module.exports = { HighlightedDriversSchoolsController }
