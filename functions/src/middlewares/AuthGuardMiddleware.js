const { Request, Response, NextFunction } = require('express')
const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const createError = require('http-errors')

const { UsersRepository } = require('../repositories/UsersRepository')

class AuthGuardMiddleware {
    static getOptions(options = {}) {
        const {
            secretOrKey = process.env.API_KEY,
            audience = process.env.API_URL,
            jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken(),
        } = options

        return {
            secretOrKey,
            audience,
            jwtFromRequest,
            ...options,
        }
    }

    /**
     * Verify if Authorization header is present on HTTP's request, and if has the "Bearer" prefix and
     * if the token is valid.
     *
     * @param {Request} req
     * @param {Response<AuthenticateResponse>} res
     * @param {NextFunction} next
     * @param {Object} [options={}]
     */
    static isAuthenticated(req, res, next, options = {}) {
        const parsedOptions = AuthGuardMiddleware.getOptions(options)

        passport.use(
            new JwtStrategy(parsedOptions, async (payload, done) => {
                try {
                    const usersRepository = new UsersRepository()
                    const user = await usersRepository.getFirst(payload.id)

                    if (!user) {
                        return next(
                            new createError.Unauthorized(
                                'Invalid credentials.'
                            ),
                            null
                        )
                    }

                    return done(null, user)
                } catch (error) {
                    console.log('AuthGuardMiddleware', error)

                    return next(error, null)
                }
            })
        )

        return passport.authenticate('jwt', { session: false })(req, res, next)
    }

    /**
     * Verify if authenticated user has the "admin" role.
     *
     * @param {Request} req
     * @param {Response<AuthenticateResponse>} res
     * @param {NextFunction} next
     * @param {Object} [options={}]
     */
    static isAdmin(req, res, next, options = {}) {
        const parsedOptions = AuthGuardMiddleware.getOptions(options)

        passport.use(
            new JwtStrategy(parsedOptions, async (payload, done) => {
                try {
                    if (payload.role !== 'admin') {
                        return next(new createError.Forbidden(), null)
                    }

                    return done(null, req.user)
                } catch (error) {
                    console.log('AuthGuardMiddleware', error)

                    return next(error, null)
                }
            })
        )

        return passport.authenticate('jwt', { session: false })(req, res, next)
    }
}

module.exports = { AuthGuardMiddleware }
