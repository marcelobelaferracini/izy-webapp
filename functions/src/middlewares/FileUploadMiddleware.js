const { join, extname } = require('path')

const multer = require('multer')
const slug = require('slug')

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        let destination = join(
            process.cwd(),
            'public',
            'images',
            'drivers-schools'
        )

        if (process.env.ENVIRONMENT === 'production') {
            destination = ''
        }

        callback(null, destination)
    },
    filename: (req, file, callback) => {
        const id = Date.now()
        const name = slug(`${id}-${file.fieldname}`)
        const extension = extname(file.originalname)
        const filename = `${name}${extension}`

        callback(null, filename)
    },
})
const externalFileUploadInstance = multer({ dest: '/', storage })

class FileUploadMiddleware {
    static upload(req, res, next) {
        next()
    }

    static single(req, res, next, options = {}) {
        const { name = 'file' } = options
        const callback = externalFileUploadInstance.single(name)

        return callback(req, res, next)
    }
}

module.exports = { FileUploadMiddleware }
