const express = require('express')

const {
    DriversSchoolsController,
} = require('../controllers/DriversSchoolsController')
const {
    DriversSchoolsReviewsController,
} = require('../controllers/DriversSchoolsReviewsController')
const {
    DriversSchoolsAddressesController,
} = require('../controllers/DriversSchoolsAddressesController')
const {
    HighlightedDriversSchoolsController,
} = require('../controllers/HighlightedDriversSchoolsController')
const { AuthGuardMiddleware } = require('../middlewares/AuthGuardMiddleware')
const { FileUploadMiddleware } = require('../middlewares/FileUploadMiddleware')

const DriversSchoolsRouter = express.Router()
const isAdminGuard = [
    (req, res, next) => {
        AuthGuardMiddleware.isAuthenticated(req, res, next)
    },
    (req, res, next) => {
        AuthGuardMiddleware.isAdmin(req, res, next)
    },
]

/**
 * Drivers schools addresses routes
 */
DriversSchoolsRouter.get('/addresses', DriversSchoolsAddressesController.index)
DriversSchoolsRouter.get(
    '/addresses/cities',
    DriversSchoolsAddressesController.getCities
)
DriversSchoolsRouter.get(
    '/addresses/neighbourhoods',
    DriversSchoolsAddressesController.getNeighbourhoods
)
DriversSchoolsRouter.get(
    '/addresses/:id',
    DriversSchoolsAddressesController.show
)

/**
 * Drivers schools reviews routes
 */
DriversSchoolsRouter.get('/reviews', DriversSchoolsReviewsController.index)
DriversSchoolsRouter.get('/reviews/:id', DriversSchoolsReviewsController.show)
DriversSchoolsRouter.delete(
    '/reviews/:id',
    ...isAdminGuard,
    DriversSchoolsReviewsController.destroy
)
DriversSchoolsRouter.put(
    '/reviews/:id',
    ...isAdminGuard,
    DriversSchoolsReviewsController.update
)
DriversSchoolsRouter.post(
    '/:id/reviews',
    ...isAdminGuard,
    DriversSchoolsReviewsController.store
)

/**
 * Highlighted drivers schools routes
 */
DriversSchoolsRouter.get(
    '/highlighted',
    HighlightedDriversSchoolsController.index
)
DriversSchoolsRouter.post(
    '/highlighted',
    ...isAdminGuard,
    HighlightedDriversSchoolsController.store
)
DriversSchoolsRouter.put(
    '/highlighted/:id',
    ...isAdminGuard,
    HighlightedDriversSchoolsController.update
)
DriversSchoolsRouter.delete(
    '/highlighted/:id',
    ...isAdminGuard,
    HighlightedDriversSchoolsController.destroy
)

/**
 * Drivers schools routes
 */
DriversSchoolsRouter.get('/', DriversSchoolsController.index)
DriversSchoolsRouter.get('/:id', DriversSchoolsController.show)
DriversSchoolsRouter.delete(
    '/:id',
    ...isAdminGuard,
    DriversSchoolsController.destroy
)
DriversSchoolsRouter.put(
    '/:id',
    ...isAdminGuard,
    (req, res, next) =>
        FileUploadMiddleware.single(req, res, next, { name: 'picture' }),
    DriversSchoolsController.update
)

DriversSchoolsRouter.post(
    '/',
    ...isAdminGuard,
    (req, res, next) =>
        FileUploadMiddleware.single(req, res, next, { name: 'picture' }),
    DriversSchoolsController.store
)

module.exports = { DriversSchoolsRouter }
