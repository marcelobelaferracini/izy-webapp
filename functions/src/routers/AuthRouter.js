const { Router } = require('express')

const { AuthController } = require('../controllers/AuthController')

const AuthRouter = Router()

AuthRouter.post('/login', AuthController.authenticate)

module.exports = { AuthRouter }
