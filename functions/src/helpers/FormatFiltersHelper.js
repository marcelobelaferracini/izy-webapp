const { useJSONParse } = require('../utils/useJSONParse')
const { RawFilters, Filters } = require('../types')

class FormatFiltersHelper {
    static operators = {
        '==': '=',
    }

    static push(filter, filters) {
        if (typeof filter !== 'string') {
            let [field, operator, value] = filter
            operator = FormatFiltersHelper.operators[operator]
                ? FormatFiltersHelper.operators[operator]
                : operator

            if (value) {
                return filters.push([field, operator, useJSONParse(value)])
            }

            return filters.push([field, operator])
        }
        let [field, operator, value] = filter.split(',')
        operator = FormatFiltersHelper.operators[operator]
            ? FormatFiltersHelper.operators[operator]
            : operator

        if (value) {
            return filters.push([field, operator, useJSONParse(value)])
        }

        filters.push([field, operator])
    }
    /**
     * Format filters.
     *
     * @param {RawFilters} rawFilters
     * @returns {Filters}
     */
    static perform(rawFilters) {
        const formattedFilters = {
            ...rawFilters,
            where: [],
            order: [],
            relationships: [],
            limit: rawFilters.limit ?? 100,
            offset: rawFilters.offset ?? 0,
        }

        if (rawFilters.where) {
            rawFilters.where.forEach((filter) => {
                FormatFiltersHelper.push(filter, formattedFilters.where)
            })
        }

        if (rawFilters.order) {
            rawFilters.order.forEach((filter) => {
                FormatFiltersHelper.push(filter, formattedFilters.order)
            })
        }

        const hasRelationships =
            rawFilters.relationships &&
            !!Object.keys(rawFilters.relationships).length

        if (hasRelationships) {
            for (let relationship in rawFilters.relationships) {
                if (rawFilters.relationships[relationship]) {
                    relationship = rawFilters.relationships[relationship]
                    FormatFiltersHelper.push(
                        relationship,
                        formattedFilters.relationships
                    )
                }
            }
        }

        return formattedFilters
    }
}

module.exports = { FormatFiltersHelper }
