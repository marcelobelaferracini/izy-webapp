#!/bin/sh

if [ $ENVIRONMENT = 'development' ]
then
    if [ ! -d 'node_modules' ]
    then
        echo '\n==== Installing base dependencies on local environment ===='
        npm install
        echo '\n==== Installing backend dependencies on local environment ===='
        npm install --prefix functions
        echo '\n==== Installing frontend dependencies on local environment ===='
        npm install --prefix frontend

        echo '\n==== Copying config files ===='
        cp -u /home/app/frontend/nuxt.config.js /home/app/functions/nuxt.config.js
        cp -u /home/app/config/firebase.json /home/app/functions/config/firebase.json
        cp -u /home/app/deploy/service-account.json /home/app/functions/config/service-account.json
    fi

    echo '\n==== Setup environment variables ===='
    cp /home/app/.env /home/app/frontend/.env
    cp /home/app/.env /home/app/functions/.env

    echo '\n==== Setup folders ===='
    mkdir -p /home/app/functions/public/images/drivers-schools
    chmod -R 775 /home/app/functions/public

    echo '\n==== Running application ===='
    npm run dev --prefix functions
else
    # npm run migrate:latest --prefix functions
    pm2 start npm --name functions -- run dev --env production --prefix functions
    pm2 start npm --name frontend -- run start --prefix frontend -- --port 80
    pm2-runtime /root/.pm2/ecosystem.config.js
fi