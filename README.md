# izy-webapp

Monorepo containg backend and frontend for Izy application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Required apps to setup the application

```bash
Docker >= 20.10.11
Docker Compose >= v2.2.1
```

### Installing

#### Clone the repository

```bash
git clone git@bitbucket.org:matheushenriquefss/izy-webapp.git
```

#### Set the environment variables

```bash
cp deploy/.env.development .env
```

Set the values at the **.env** file according to your needs

#### Setup Nginx

Add the following host to your virtualhost file:

```bash
127.0.0.1       api.izy-drive.dev.com
```

Set the virtualhost file:

```bash
cp deploy/nginx/virtualhost.development.conf deploy/nginx/virtualhost.conf
```

#### Start the backend service

```bash
docker compose up
```

#### Create the database

```bash
docker compose exec db mysql -u root -p
```

Type **root** as password and run:

```bash
create database `izy-drive`;
```

After the database is created run:

```bash
exit
```

#### Running migrations

```bash
docker compose exec izy-webapp npm run migrate:latest --prefix functions
```

#### Populate the database

Run, inside the container:

```bash
cd functions/src/commands
node parseAndStoreDrivingSchools
```

#### Start the frontend service

To get the frontend up and running you'll need to:

```bash
docker compose exec izy-webapp npm run dev --prefix frontend
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Run code Lint

```bash
docker compose exec izy-webapp npm run lint
```

## Deployment

```bash
docker compose exec izy-webapp npm run deploy --prefix functions
```

## Built With

-   [Nuxt 2.15.8](https://nuxtjs.org/) - Front-end framework
-   [ExpressJS](https://expressjs.com/) - Back-end infrastructure
