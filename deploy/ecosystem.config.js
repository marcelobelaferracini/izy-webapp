module.exports = {
  apps: [
    {
      name: 'functions',
      script: 'npm',
      args: 'run dev --env production --prefix functions',
    },
    {
      name: 'frontend',
      script: 'npm',
      args: 'run start --prefix frontend -- --port 80',
    },
  ],
};