export default function ({ $axios }) {
    $axios.onRequest((request) => {
        const accessToken = window.sessionStorage.getItem('access-token')
        if (accessToken) {
            request.headers.Authorization = `Bearer ${accessToken}`
        }
    })
}
