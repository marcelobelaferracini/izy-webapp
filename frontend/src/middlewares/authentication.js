import jwt_decode from 'jwt-decode'

export default function ({ store, route, redirect }) {
    const isProtected = route.path.startsWith('/admin')

    if (!process.server) {
        const accessToken = window.sessionStorage.getItem('access_token')

        if (isProtected) {
            const user = store.getters['user/getUser']

            if (!accessToken) return redirect('/login')

            if (!user) {
                const { id, email, role } = jwt_decode(accessToken)
                store.commit('user/setUser', { id, email, role })
            }
        }

        return
    }

    const user = store.getters['user/getUser']

    if (isProtected && !user) return redirect('/login')
}
