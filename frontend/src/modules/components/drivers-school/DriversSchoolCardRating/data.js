export const stars = {
    1: ['star-half', 'star', 'star', 'star', 'star'],
    1.5: ['star-fill', 'star-half', 'star', 'star', 'star'],
    2: ['star-fill', 'star-fill', 'star', 'star', 'star'],
    2.5: ['star-fill', 'star-fill', 'star-half', 'star', 'star'],
    3: ['star-fill', 'star-fill', 'star-fill', 'star', 'star'],
    3.5: ['star-fill', 'star-fill', 'star-fill', 'star-half', 'star'],
    4: ['star-fill', 'star-fill', 'star-fill', 'star-fill', 'star'],
    4.5: ['star-fill', 'star-fill', 'star-fill', 'star-fill', 'star-half'],
    5: ['star-fill', 'star-fill', 'star-fill', 'star-fill', 'star-fill'],
}
