import { useAxios } from '~/modules/hooks/commons/useAxios'

export const useFetchDriversSchoolBySlug = async (slug) => {
    const $axios = useAxios()
    const { data } = await $axios.get(
        `/drivers-schools?where[]=slug,==,${slug}&relationships[]=drivers_schools_addresses`
    )
    return data[0]
}
