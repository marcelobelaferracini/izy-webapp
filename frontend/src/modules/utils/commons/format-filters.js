export const formatFilters = () => {
    const filters = {
        cidade: 'where[]=drivers_schools_addresses.city,=,',
        bairro: 'where[]=drivers_schools_addresses.neighbourhood,=,',
        cnh: 'where[]=licenses,=,',
        'ordenar-por': 'order[]=',
    }
    const sortings = {
        'menor-preco': 'average_price_rate,ASC',
        'melhores-avaliados': 'average_rate,DESC',
    }
    let formattedFilters = window.location.search

    for (const [key, value] of Object.entries(filters)) {
        formattedFilters = formattedFilters.replace(`${key}=`, value)
    }

    for (const [key, value] of Object.entries(sortings)) {
        formattedFilters = formattedFilters.replace(key, value)
    }

    formattedFilters = new URLSearchParams(formattedFilters)
    const driversSchoolAddressFilters = ['city', 'neighbourhood']

    driversSchoolAddressFilters.forEach((filter) => {
        if (formattedFilters.toString().includes(filter)) {
            formattedFilters.set('relationships[]', 'drivers_schools_addresses')
        }
    })

    return formattedFilters
}
