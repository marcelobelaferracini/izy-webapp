const websiteSchema = (options = {}) => ({
    '@context': 'http://schema.org',
    '@type': 'WebSite',
    url: options.url ?? 'https://izydrive.com.br',
    potentialAction: options.potentialAction ?? [],
})

const webPageSchema = (options = {}) => ({
    '@context': 'http://schema.org',
    '@type': 'WebPage',
    name: options.name,
    url: options.url,
    description: options.description,
})

const organizationSchema = (options = {}) => ({
    '@context': 'http://schema.org',
    '@type': 'Organization',
    name: 'IzyDrive',
    url: options.url ?? 'https://izydrive.com.br',
    logo: options.logo ?? 'https://izydrive.com.br/images/izydrive-dark.png',
    description:
        'A IzyDrive é um agregador de autoescolas que visa descomplicar os processos de formação de novos condutores, de reciclagem e de renovação da carteira  auxiliando na escolha da melhor autoescola para seu perfil. Usamos tecnologia para garantir uma experiência segura em todas as etapas do processo. Desde o momento da busca pela autoescola mais próxima, com melhor preço ou melhor avaliada até o contato com a autoescola, tudo é feito online, sem precisar sair de casa. Oferecemos um sistema de busca inteligente que identifica quais autoescolas estão mais próximas de você.',
    sameAs: [],
})

const itemListSchema = (options = {}) => ({
    '@context': 'http://schema.org',
    '@type': 'ItemList',
    url: options.url ?? '',
    numberOfItems: options.numberOfItems ?? 0,
    itemListElement: options.itemListElement ?? [],
})

export { websiteSchema, webPageSchema, organizationSchema, itemListSchema }
