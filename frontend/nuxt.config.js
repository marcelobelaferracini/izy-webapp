module.exports = {
    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'IzyDrive',
        htmlAttrs: {
            lang: 'pt-BR',
        },
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content:
                    'Encontre autoescolas para aulas práticas, teóricas, reciclagem, renovação de habilitação/CNH com o menor preço. Entre em contato com a autoescola de forma descomplicada. Busque grátis!',
            },
            { name: 'format-detection', content: 'telephone=no' },
            {
                name: 'google-site-verification',
                content: '_RH5tVtRIf33Bd7tZ7Ww2vatbjgjdBQhuXCefwkn6dk',
            },
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon-light.ico',
                media: '(prefers-color-scheme:light)',
            },
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon-dark.ico',
                media: '(prefers-color-scheme:dark)',
            },
        ],
        script: [
            {
                src: 'https://www.googletagmanager.com/gtag/js?id=G-BT5T250Y10',
                async: true,
            },
            {
                src: '/javascripts/analytics.js',
                async: true,
            },
            {
                src: '/javascripts/hotjar.js',
                defer: true,
            },
        ],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: ['../src/assets/stylesheets/app.scss'],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '../src/plugins/directives',
        '../src/plugins/axios.client',
        '../src/plugins/dropdown.client',
        '../src/plugins/offcanvas.client',
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        '@nuxtjs/composition-api/module',
        '@nuxt/image',
        '@nuxtjs/svg',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
        baseURL: process.env.BASE_URL,
        headers: { 'Content-Type': 'application/json' },
    },

    image: {
        domains: [
            'firebasestorage.googleapis.com',
            'localhost',
            'api.izy-drive.dev.com',
        ],
    },

    srcDir: 'src',

    dir: {
        pages: 'views',
        static: '../public',
        components: 'modules/components',
        middleware: 'middlewares',
        store: 'stores',
    },

    publicRuntimeConfig: {
        BASE_URL: process.env.API_URI,
    },

    router: {
        middleware: ['authentication'],
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        loaders: {
            vue: {
                compilerOptions: {
                    experimentalCompatMode: 2,
                    experimentalDisableTemplateSupport: true,
                },
            },
        },
        extend(config) {
            config.resolve.alias['~config'] = '../../../config'
        },
    },
}
